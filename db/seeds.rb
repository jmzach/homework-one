# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Band.delete_all
Club.delete_all
Booking.delete_all

bands = Band.create([{name: "Enter Shikari", num_members: 4},{name: "Disarmonia Mundi", num_members: 3},{name: "In Flames", num_members: 5},{name: "A Day to Remember", num_members: 5}])
clubs = Club.create([{name: "Scout Bar", street_address: "18307 S Egret Bay Blvd  Houston, TX 77058"},{name: "House of Blues", street_address: "1204 Caroline St, Houston, TX 77002"}])


Booking.create([{:band_id => 0, :club_id => 0, :fee => 50.00}, {:band_id => 1, :club_id => 1, :fee => 125.00}, {:band_id => 2, :club_id => 1, :fee => 60.00}, {:band_id => 3, :club_id => 0, :fee => 55.00}])