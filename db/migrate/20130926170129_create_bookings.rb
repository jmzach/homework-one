class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :band_id
      t.integer :club_id
      t.float :fee

      t.timestamps
    end
    add_index :bookings, :band_id
    add_index :bookings, :club_id



    end

  end

